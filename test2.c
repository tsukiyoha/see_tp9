#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(void)
{
    int fd_create, fd_read, fd_write, fd_rdwr;

    fd_create = open("text_create",O_WRONLY | O_CREAT);
    fd_read = open("text_read", O_RDONLY);
    fd_write = open("text_write", O_WRONLY);
    fd_rdwr = open("text_rdwr", O_RDWR);

    close(fd_rdwr);
    close(fd_read);
    close(fd_write);
    close(fd_create);
    return 0;
}