obj-m += m2lse_module.o
# Racine des sources du noyau :
KERN_DIR=/home/kerboitk/Documents/Cours/Master2/see/see_tp6/buildroot-2018.02.7/output/build/linux-4.13.13
# Architecture cible :
ARCH=arm
# Prefixe des outils de developpement :
CROSS_COMPILE=/home/kerboitk/Documents/Cours/Master2/see/see_tp6/buildroot-2018.02.7/output/host/usr/bin/arm-linux-

all:
	make -C $(KERN_DIR) M=$(PWD) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) modules
	# cleanup
	rm -rf *.o *.mod.c modules.order Module.symvers

clean:
	make -C $(KERN_DIR) M=$(PWD) ARCH=$(ARCH) CROSS_COMPILE=$(CROSS_COMPILE) clean
