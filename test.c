#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


int main(void)
{
    int pid;

    printf("SAME ANYONE : What if I give birth !\n");
    pid = fork();

    if(pid == 0)
    {
        printf("CHILD : I am the child of the ...\n");
        sleep(3);
        printf("CHILD : DEAD?\n");
        return 0;

    }
    else
    {
        printf("SAME ANYONE : I am your father/mother/whatever !\n");
    }

    wait(NULL);

    printf("STILL ANYONE : Oh! it's dead... RIP...\n");

    printf("SAME ANYONE : let's try again !\n");
    pid = fork();

    if(pid == 0)
    {
        printf("CHILD : I am the second (?) child of the ...\n");
        sleep(3);
        printf("CHILD : DEAD?\n");
        return 0;

    }
    else
    {
        printf("SAME ANYONE : I am your new father/mother/whatever !\n");
    }

    wait(NULL);

    printf("STILL ANYONE : Oupsie! it's dead again... RIP...\n");
    
    return 0;
}