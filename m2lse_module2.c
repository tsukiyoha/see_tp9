/*
* Here's a sample kernel module showing the use of jprobes to dump
* the arguments of _do_fork().
*
* For more information on theory of operation of jprobes, see
* Documentation/kprobes.txt
*/

 #include <linux/kernel.h>
 #include <linux/module.h>
 #include <linux/kprobes.h>
 
 /*
 * Jumper probe for _do_fork.
 * Mirror principle enables access to arguments of the probed routine* from the probe handler.
 */

/* Handlerhaving the same arguments as actual _do_fork() routine */
static long handler_do_sys_open(int dfd, const char __user *filename, int flags, umode_t mode)
{
    printk(KERN_INFO "jprobe: pathname = %s, flags = %d, mode = %d\n", pathname, flags, mode);
    /* Always end with a call to jprobe_return(). */
    jprobe_return();
    return 0;
}

static struct jprobe my_jprobe = {
    .entry= handler_do_sys_open,
    .kp = {
        .symbol_name= "do_sys_open",
        },
    };
    
static int __init jprobe_init(void)
{
    int ret;
    ret = register_jprobe(&my_jprobe);
    
    if (ret < 0)
    {
        pr_err("register_jprobe failed, returned %d\n", ret);
        return -1;
    }
    printk(KERN_INFO "Planted jprobe at %p, handler addr %p\n",my_jprobe.kp.addr, my_jprobe.entry);
    return 0;
}

static void __exit jprobe_exit(void)
{
    unregister_jprobe(&my_jprobe);
    printk(KERN_INFO "jprobe at %p unregistered\n", my_jprobe.kp.addr);
}

module_init(jprobe_init)
module_exit(jprobe_exit)

MODULE_LICENSE("GPL");